const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', ()=>{
    context('Function isUserNameValid', ()=>{
        it('Function prototype : boolean isUserNameValid(username: String)', ()=>{
            expect(validate.isUserNameValid('win')).to.be.true;
        });

        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', ()=>{
            expect(validate.isUserNameValid('aa')).to.be.false;
        });

        it('ทุกตัวต้องเป็นตัวเล็ก', ()=>{
            expect(validate.isUserNameValid('aAa')).to.be.false;
            expect(validate.isUserNameValid('aaA')).to.be.false;
            expect(validate.isUserNameValid('aAA')).to.be.false;
        });

        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', ()=>{
            expect(validate.isUserNameValid('123456789123456')).to.be.true;
            expect(validate.isUserNameValid('1234567891234567')).to.be.false;
        });
    });

    context('Function isAgeValid ', ()=>{
        it('Function prototype : boolean isAgeValid (age: String)', ()=>{
            expect(validate.isAgeValid('18')).to.be.true;
        });

        it('age ต้องเป็นข้อความที่เป็นตัวเลข', ()=>{
            //expect(validate.isAgeValid('000')).to.be.true;
            expect(validate.isAgeValid('a')).to.be.false;
            expect(validate.isAgeValid('a2')).to.be.false;
            expect(validate.isAgeValid('20')).to.be.true;
        });

        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', ()=>{
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('18')).to.be.true;
            expect(validate.isAgeValid('100')).to.be.true;
            expect(validate.isAgeValid('101')).to.be.false;
        });
    });

    context('Function isPasswordValid  ', ()=>{
        it('Function prototype : boolean isUserNameValid(password: String)', ()=>{
           expect(validate.isPasswordValid('Password+1')).to.be.true;
        });

        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', ()=>{
            expect(validate.isPasswordValid('pass')).to.be.false;
            expect(validate.isPasswordValid('Pass+123456')).to.be.true;
         });

         it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', ()=>{
            expect(validate.isPasswordValid('Password+1123')).to.be.true;
            expect(validate.isPasswordValid('PAssword+111')).to.be.true;
            expect(validate.isPasswordValid('password')).to.be.false;
         });

         it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', ()=>{
            expect(validate.isPasswordValid('Password+123')).to.be.true;
            expect(validate.isPasswordValid('+PAssword2222')).to.be.true;
            expect(validate.isPasswordValid('password33')).to.be.false;
            expect(validate.isPasswordValid('password')).to.be.false;
         });

         it('ต้องมีอักขระ พิเศษ อย่างน้อย 1 ตัว', ()=>{
            expect(validate.isPasswordValid('Passs11111')).to.be.false;
            expect(validate.isPasswordValid('Pass+123456')).to.be.true;
         });

    });

    context('Function isDateValid  ', ()=>{
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', ()=>{
            expect(validate.isDateValid('18','1','1999')).to.be.true;
        });

        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', ()=>{
            expect(validate.isDateValid('181','1','1999')).to.be.false;
        });

        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', ()=>{
            expect(validate.isDateValid('11','14','1999')).to.be.false;
            expect(validate.isDateValid('181','16','1999')).to.be.false;
        });

        it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020', ()=>{
            expect(validate.isDateValid('11','12','12060')).to.be.false;
            expect(validate.isDateValid('181','1','1940')).to.be.false;
        });

        it('เดือนที่ลงท้ายด้วย คม day ต้องเริ่มที่1 และไม่เกิน31', ()=>{
            expect(validate.isDateValid('33','1','1970')).to.be.false;
            expect(validate.isDateValid('31','1','1970')).to.be.true;
        });

        it('เดือนที่ลงท้ายด้วย ยน day ต้องเริ่มที่1 และไม่เกิน30', ()=>{
            expect(validate.isDateValid('33','4','1970')).to.be.false;
            expect(validate.isDateValid('31','9','1970')).to.be.false;
        });

        it('เดือนกุมภาพันธ์ day ต้องเริ่มที่1 และไม่เกิน28', ()=>{
            expect(validate.isDateValid('31','2','1970')).to.be.false;
            expect(validate.isDateValid('28','2','1970')).to.be.true;
        });

        it('ในกรณีของปี อธิกสุรทิน ให้คำนวนตามหลักเกณฑ์นี้ในกรณีของปี อธิกสุรทิน ให้คำนวนตามหลักเกณฑ์นี้', ()=>{
            expect(validate.isDateValid('29','2','2002')).to.be.false;
            expect(validate.isDateValid('29','2','2000')).to.be.true;
        });


    });
});