module.exports = {
    isUserNameValid: function(username){
        if(username.length<3 || username.length>15){
            return false;
        }
        if(username.toLowerCase()!==username){
            return false;
        }
        return true;
    },

    isAgeValid: function(age){
        var digit = parseInt(age);
        if(isNaN(age)){
            return false;
        }
        else if(age!=parseInt(age)){
            return false;
        }
        else if(digit < 18 || digit > 100){
            return false;
        }
        return true;
    },

    isPasswordValid: function(password){
        var special =0;
        var i;
        var a;
        if(password.length<8){
            return false;
        }
        if(password.toLowerCase()==password){
            return false;
        }
        var digit = parseInt(password);
        if(digit>=3){
            return true;
        }
        for(i = 0;i<password.length;i++){
            a = password.charCodeAt(i);
            if((a>=33&&a<=47)){
                special++;
            }
            

        
        }
        if(special<1){
                return false;
            }
        return true;
    },
    
    isDateValid: function(day,month,year){
        if(day<1||day>31){
            return false;
        }else if(month<1||month>12){
            return false;
        }else if(year<1970||year>2020){
            return false;
        }else if(month == 1 || month == 3 || 
            month== 5 || month == 7 || 
            month == 8 || month == 10 || 
            month == 12){
    if(day < 1 || day > 31){
        return false;
    }
}else if(month == 4 || month == 6 || 
    month == 9 ||month == 11){
    if(day < 1 || day > 30){
        return false;
    }    
}else if(month == 2){
    if(year % 100 == 0 && year  % 400 == 0){
        if(day < 1 || day > 29){
            return false;
        }
    }else{
        if(day < 1 || day > 28){
            return false;
        }
    }
}

        return true;
    }

}